from rest_framework import permissions
from movies_api.settings import API_KEY
from films_catalogue.exceptions import APIKeyNotProvided


class IsNotAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view) -> bool:
        if request.user.is_authenticated:
            return False
        return True


class APIKeyAuth(permissions.BasePermission):
    def has_permission(self, request, view) -> bool:
        if request.headers.get("X-API-Key") == API_KEY:
            return True
        return False
