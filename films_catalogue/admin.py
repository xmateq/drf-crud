from django.contrib import admin
from films_catalogue.models import Film, Comment


@admin.register(Film)
class FilmModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Comment)
class CommentModelAdmin(admin.ModelAdmin):
    pass
