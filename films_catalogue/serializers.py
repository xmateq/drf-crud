from rest_framework.serializers import ModelSerializer
from films_catalogue.models import Film, Comment
from django.contrib.auth import get_user_model

User = get_user_model()


class FilmSerializer(ModelSerializer):
    class Meta:
        model = Film
        fields = ('title', 'poster', 'rate', 'genre', 'director',)


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password',)
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password')
        return User.objects.create_user(**validated_data, password=password)


class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = ('content', 'film', 'author',)
        extra_kwargs = {'author': {'read_only': True}, 'film': {'read_only': True}}
