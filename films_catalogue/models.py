from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Film(models.Model):
    title = models.CharField(max_length=100)
    poster = models.URLField(max_length=400)
    rate = models.FloatField()
    genre = models.CharField(max_length=100)
    director = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Comment(models.Model):
    content = models.TextField(max_length=500)
    film = models.ForeignKey(Film, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.content
