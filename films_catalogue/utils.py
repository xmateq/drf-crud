import requests
import jmespath
from bs4 import BeautifulSoup
from films_catalogue.models import Film
from movies_api.settings import MOVIES_URL_TEMPLATE, PAGES_TO_GET, MOVIES_ATTEMPT_LIMIT, API_KEY


def find_movies(page: int):
    for _ in range(MOVIES_ATTEMPT_LIMIT):
        response = requests.get(MOVIES_URL_TEMPLATE.format(page), API_KEY)
        if response.status_code == requests.codes.ok:
            soup = BeautifulSoup(response.content, 'html.parser')
            films = soup.find(class_="resultsList hits")
            if films:
                return films
            return []
        raise Exception("Invalid response received")


def get_movie_property_text(movie, classname: str) -> str:
    result = movie.find(class_=classname)
    if result:
        return result.get_text()
    return ''


def find_movie_poster(movie) -> list:
    return jmespath.search('content', movie.find(class_='poster__image')) or []


def find_movie_title(movie) -> str:
    return get_movie_property_text(movie, 'filmPreview__title') or []


def find_movie_rate(movie) -> float:
    return float(get_movie_property_text(movie, 'rateBox__rate').replace(',', '.')) or 0.0


def find_movie_genre(movie) -> list:
    try:
        return [genre.get_text() for genre in movie.find(
            class_='filmPreview__info filmPreview__info--genres').find_all('a')]
    except AttributeError:
        return []


def find_movie_director(movie) -> list:
    try:
        return [director.get_text() for director in movie.find(
            class_='filmPreview__info filmPreview__info--directors').find_all('a')]
    except AttributeError:
        return []


def get_movie(movie) -> Film:
    return Film(
        title=find_movie_title(movie),
        poster=find_movie_poster(movie),
        rate=find_movie_rate(movie),
        genre=find_movie_genre(movie),
        director=find_movie_director(movie)
    )


def final_results(page: int) -> list:
    return [get_movie(movie) for movie in find_movies(page)]


def find_movies_of_n_pages() -> list:
    all_films = []
    lists_of_models_list = [final_results(page + 1) for page in range(0, PAGES_TO_GET)]
    for single_list in lists_of_models_list:
        for single_model in single_list:
            all_films.append(single_model)
    return Film.objects.bulk_create(all_films)
