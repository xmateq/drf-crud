from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveUpdateAPIView, DestroyAPIView
from films_catalogue.models import Film, Comment
from films_catalogue.serializers import FilmSerializer, UserSerializer, CommentSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.contrib.auth import get_user_model
from films_catalogue.permissions import IsNotAuthenticated
from rest_framework.authentication import TokenAuthentication, BasicAuthentication
from films_catalogue.permissions import APIKeyAuth
from films_catalogue.exceptions import UserUnauthorizedException
from django.shortcuts import get_object_or_404
from films_catalogue.utils import find_movies_of_n_pages
from rest_framework.response import Response
from rest_framework import status

User = get_user_model()


class FilmAbstractApiView(object):
    queryset = Film.objects.all()
    serializer_class = FilmSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)


class FilmApiView(FilmAbstractApiView, ListAPIView):
    pass


class FilmCreateApiView(FilmAbstractApiView, CreateAPIView):
    pass


class FilmUpdateApiView(FilmAbstractApiView, RetrieveUpdateAPIView):
    pass


class FilmDeleteApiView(FilmAbstractApiView, DestroyAPIView):
    pass


class FilmScrapeApiView(APIView):
    permission_classes = (APIKeyAuth,)

    def get(self, *args, **kwargs):
        find_movies_of_n_pages()
        return Response({}, status=status.HTTP_200_OK)


class UserRegisterView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsNotAuthenticated,)
    authentication_classes = (BasicAuthentication,)


class CommentAbstractApiView(object):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticated,)


class CommentCreateApiView(CommentAbstractApiView, CreateAPIView):

    def perform_create(self, serializer, **kwargs):
        user = self.request.user
        if not user.is_authenticated:
            raise UserUnauthorizedException
        film = get_object_or_404(Film, pk=self.kwargs.get('pk'))
        return serializer.save(author=user, film=film)


class MyCommentsView(CommentAbstractApiView, ListAPIView):

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return Comment.objects.filter(author=self.kwargs['pk'])
        raise UserUnauthorizedException
