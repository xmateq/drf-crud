from django.apps import AppConfig


class CrudConfig(AppConfig):
    name = 'films_catalogue'
