from django.urls import path, include
from films_catalogue import views as films_catalogue_views

urlpatterns = [
    path('films/create/', films_catalogue_views.FilmCreateApiView.as_view()),
    path('films/', films_catalogue_views.FilmApiView.as_view()),
    path('films/<int:pk>/', films_catalogue_views.FilmUpdateApiView.as_view()),
    path('films/<int:pk>/delete/', films_catalogue_views.FilmDeleteApiView.as_view()),
    path('users/', include('djoser.urls.authtoken')),
    path('users/register/', films_catalogue_views.UserRegisterView.as_view()),
    path('films/<int:pk>/comment/', films_catalogue_views.CommentCreateApiView.as_view()),
    path('users/<int:pk>/comments/', films_catalogue_views.MyCommentsView.as_view()),
    path('films/scrape/', films_catalogue_views.FilmScrapeApiView.as_view()),
]
