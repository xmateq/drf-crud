from rest_framework import status
from rest_framework.exceptions import APIException


class UserUnauthorizedException(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "You don't have enough permissions, please log in or register"
    default_code = 'unauthorized'


class APIKeyNotProvided(APIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = "API Key not provided"
    default_code = "forbidden"
